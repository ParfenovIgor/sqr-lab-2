# Lab 2 -- Linter and SonarQube as a part of quality gates

[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-2/badges/master/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-2/-/commits/master)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## SonarQube

Install *SonarQube* through *docker*:

`docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest`

Do configuration and get this picture:

![SonarQube](/screenshots/1.png)

## SonarCloud

Create account at [SonarCloud](https://sonarcloud.io/).

Create access token and add to *CI* as environmental variable.

Create organization and enter it into `pom.xml`.

The result on shown on site:

![SonarCloud](/screenshots/2.png)
